import { JdownloaderClient, Device, AddLink } from '../src';

// tslint:disable-next-line
const { config } = require('./config');

describe('JdownloderAPI', () => {
  let api: JdownloaderClient;

  beforeAll(async () => {
    api = new JdownloaderClient(config.username, config.password);
    await api.core.connect();
  });

  beforeEach(async () => {
    await new Promise(resolve => setTimeout(resolve, 1000));
  });

  describe('Test core', () => {
    test.skip('should connect', async () => {
      expect(await api.core.connect()).toBeTruthy();
    });

    test.skip('should list devices', async () => {
      const devices = await api.core.listDevices();
      expect(devices).toHaveLength(1);
      expect(devices[0]).toHaveProperty('id');
      expect(devices[0]).toHaveProperty('type');
      expect(devices[0]).toHaveProperty('status');
      expect(devices[0]).toHaveProperty('name');
    });

    test.skip('should disconnect', async () => {
      await api.core.listDevices();
      await api.core.disconnect();
      expect(api.core.listDevices()).rejects.toThrow();
      // reconnect
      await api.core.connect();
    });

    test.skip('should reconnect', async () => {
      await api.core.listDevices();
      await api.core.reconnect();
      await api.core.listDevices();
    });
  });

  describe('Test device', () => {
    test.skip('should get device infos', async () => {
      const device = (await api.core.listDevices())[0];
      const result = await api.device.getDirectConnectionInfos(device.id);
      expect(result).toBeDefined();
    });
  });

  describe('Test downloads', () => {
    let device: Device;

    beforeAll(async () => {
      device = (await api.core.listDevices())[0];
    });

    test.skip('should list downloads', async () => {
      const links = await api.downloadsV2.queryLinks(device.id);
      expect(links).toBeDefined();
    });

    test.skip('should list packages', async () => {
      const result = await api.downloadsV2.queryPackages(device.id);
      expect(result).toBeDefined();
    });

    test.skip('should rename link', async () => {
      const newName = `toto-${new Date().getTime()}.pdf`;
      await api.downloadsV2.renameLink(device.id, {
        linkId: 1578420962140, // TO Change
        newName,
      });

      const links = await api.downloadsV2.queryLinks(device.id);
      expect(links.find(l => l.name === newName)).toBeDefined();
    });
  });

  describe('Test linkGrabber', () => {
    let device: Device;

    beforeAll(async () => {
      device = (await api.core.listDevices())[0];
    });

    test.skip('should list links', async () => {
      const links = await api.linkgrabberV2.queryLinks(device.id);
      expect(links).toBeDefined();
    });

    test.skip('should add link', async () => {
      const request = new AddLink();
      request.autostart = true;
      request.links = 'https://sebsauvage.net/pdf/pdfgratuit.pdf';
      const links = await api.linkgrabberV2.addLinks(device.id, request);
      expect(links).toBeDefined();
    });
  });
});
