import { JdownloaderClientCore } from '../core/core';
import { JdownloaderClientDownloadsV2 } from './downloads-v2';
import { LinkQuery, PackageQuery, RenameLink } from '../requests';
import { DownloadLink, FilePackage, Priority } from '../models';

jest.mock('../core/core');

describe('JdownloaderClient DownloadsV2', () => {
  let core: JdownloaderClientCore;
  let downloadsV2: JdownloaderClientDownloadsV2;

  beforeEach(() => {
    core = new JdownloaderClientCore('', '');
    downloadsV2 = new JdownloaderClientDownloadsV2(core);
  });

  it('should cleanup', async () => {
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: null,
    });

    await downloadsV2.cleanup('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadsV2/cleanup', 'deviceId', [
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
    ]);
  });

  it('should /packageCount', async () => {
    const count = 123;
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: count,
    });

    const result = await downloadsV2.packageCount('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadsV2/packageCount', 'deviceId');
    expect(result).toEqual(count);
  });

  it('should /queryLinks', async () => {
    const links: DownloadLink[] = [
      {
        addedDate: 123,
        bytesLoaded: 123,
        bytesTotal: 1234,
        enabled: true,
        finished: false,
        host: 'AZ',
        name: 'mockedTest',
        packageUUID: 1234,
        status: 'RUNNING',
        url: 'url',
        uuid: 12345,
      },
    ];
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: links,
    });

    const result = await downloadsV2.queryLinks('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadsV2/queryLinks', 'deviceId', [
      JSON.stringify(new LinkQuery()),
    ]);
    expect(result).toEqual(links);
  });

  it('should /queryPackages', async () => {
    const packages: FilePackage[] = [
      {
        activeTask: 'activeTask',
        bytesLoaded: 123,
        bytesTotal: 1234,
        childCount: 12,
        comment: 'comment',
        downloadPassword: 'downloadPassword',
        enabled: false,
        eta: 123,
        finished: false,
        hosts: ['host'],
        name: 'name',
        priority: Priority.DEFAULT,
        running: true,
        saveTo: '/tmp',
        speed: 123,
        status: 'RUNNING',
        statusIconKey: 'statusIconKey',
        uuid: 1245,
      },
    ];
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: packages,
    });

    const result = await downloadsV2.queryPackages('deviceId');

    expect(core.callAction).toHaveBeenCalledWith('/downloadsV2/queryPackages', 'deviceId', [
      JSON.stringify(new PackageQuery()),
    ]);
    expect(result).toEqual(packages);
  });

  it('should /renameLink', async () => {
    const renameLink: RenameLink = {
      linkId: 123,
      newName: 'new name',
    };
    jest.spyOn(core, 'callAction').mockResolvedValue({
      rid: 123,
      data: null,
    });

    await downloadsV2.renameLink('deviceId', renameLink);

    expect(core.callAction).toHaveBeenCalledWith('/downloadsV2/renameLink', 'deviceId', [
      renameLink.linkId,
      renameLink.newName,
    ]);
  });
});
