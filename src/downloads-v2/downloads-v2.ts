import { JdownloaderClientCore } from '../core/core';
import { CleanUp, LinkQuery, PackageQuery, RenameLink } from '../requests';
import { FilePackage, DownloadLink } from '../models';

export class JdownloaderClientDownloadsV2 {
  constructor(protected readonly core: JdownloaderClientCore) {}

  async queryLinks(deviceId: string, linkQuery: LinkQuery = new LinkQuery()): Promise<DownloadLink[]> {
    const params = [JSON.stringify(linkQuery)];
    return (await this.core.callAction<DownloadLink[]>('/downloadsV2/queryLinks', deviceId, params)).data;
  }

  async queryPackages(deviceId: string, packageQuery: PackageQuery = new PackageQuery()): Promise<FilePackage[]> {
    const params = [JSON.stringify(packageQuery)];
    return (await this.core.callAction<FilePackage[]>('/downloadsV2/queryPackages', deviceId, params)).data;
  }

  async cleanup(deviceId: string, cleanup: CleanUp = new CleanUp()): Promise<void> {
    const params = [cleanup.linkIds, cleanup.packageIds, cleanup.action, cleanup.mode, cleanup.selectionType];
    await this.core.callAction('/downloadsV2/cleanup', deviceId, params);
  }

  async packageCount(deviceId: string): Promise<number> {
    return (await this.core.callAction<number>('/downloadsV2/packageCount', deviceId)).data;
  }

  async renameLink(deviceId: string, renameLink: RenameLink): Promise<void> {
    const params = [renameLink.linkId, renameLink.newName];
    await this.core.callAction<void>('/downloadsV2/renameLink', deviceId, params);
  }
}
