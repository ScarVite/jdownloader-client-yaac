import { JdownloaderClientCore } from './core/core';
import { JdownloaderClientDevice } from './device/device';
import { JdownloaderClientDownloadsV2 } from './downloads-v2/downloads-v2';
import { JdownloaderClientLinkgrabberV2 } from './linkgrabber-v2/linkgrabber-v2';
import { JdownloaderClientDownloadController } from './download-controller/download-controller';

export * from './models';
export * from './requests';

export class JdownloaderClient {
  readonly core: JdownloaderClientCore;
  readonly device: JdownloaderClientDevice;
  readonly downloadController: JdownloaderClientDownloadController;
  readonly downloadsV2: JdownloaderClientDownloadsV2;
  readonly linkgrabberV2: JdownloaderClientLinkgrabberV2;

  constructor(email: string, password: string) {
    this.core = new JdownloaderClientCore(email, password);
    this.device = new JdownloaderClientDevice(this.core);
    this.downloadsV2 = new JdownloaderClientDownloadsV2(this.core);
    this.linkgrabberV2 = new JdownloaderClientLinkgrabberV2(this.core);
    this.downloadController = new JdownloaderClientDownloadController(this.core);
  }
}
