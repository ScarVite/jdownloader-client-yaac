import { SelectionType } from '../models/selection-type.enum';
import { Mode } from '../models/mode.enum';
import { Action } from '../models/action.enum';

export class CleanUp {
  linkIds: number[];
  packageIds: number[];
  action: Action;
  mode: Mode;
  selectionType: SelectionType;
}
