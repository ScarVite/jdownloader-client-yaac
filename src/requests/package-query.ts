export class PackageQuery {
  bytesLoaded: boolean = true;
  bytesTotal: boolean = true;
  childCount: boolean = true;
  comment: boolean = true;
  enabled: boolean = true;
  eta: boolean = true;
  finished: boolean = true;
  hosts: boolean = true;
  maxResults: number = -1;
  packageUUIDs: number[] = [];
  priority: boolean = true;
  running: boolean = true;
  saveTo: boolean = true;
  speed: boolean = true;
  startAt: number = 0;
  status: boolean = true;
}
