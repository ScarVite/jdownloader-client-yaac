import { JdownloaderClientCore } from '../core/core';
import { DirectConnectionInfos } from '../models';

export class JdownloaderClientDevice {
  constructor(protected readonly core: JdownloaderClientCore) {}

  async getDirectConnectionInfos(deviceId: string): Promise<DirectConnectionInfos> {
    return (await this.core.callAction<DirectConnectionInfos>('/device/getDirectConnectionInfos', deviceId)).data;
  }
}
