import axios, { AxiosRequestConfig } from 'axios';
import { Device, DataResponse, RidResponse, ConnectResponse, DevicesResponse } from '../models';

import * as crypto from 'crypto';
import * as querystring from 'querystring';

import { CryptoUtils } from '../utils/crypto';
import { CoreUtils } from '../utils/core';

const ENDPOINT = 'https://api.jdownloader.org';
const APPKEY = 'my_jd_nodeJS_webinterface';
const SERVER_DOMAIN = 'server';
const DEVICE_DOMAIN = 'device';
const API_VER = 1;

export class JdownloaderClientCore {
  protected loginSecret: Buffer;
  protected deviceSecret: Buffer;
  protected sessionToken: string | null;
  protected regainToken: string | null;
  protected serverEncryptionToken: Buffer | null;
  protected deviceEncryptionToken: Buffer | null;
  protected email: string;

  constructor(email: string, password: string) {
    this.loginSecret = CryptoUtils.sha256(email + password + SERVER_DOMAIN);
    this.deviceSecret = CryptoUtils.sha256(email + password + DEVICE_DOMAIN);
    this.sessionToken = null;
    this.regainToken = null;
    this.serverEncryptionToken = null;
    this.deviceEncryptionToken = null;
    this.email = email;
  }

  /**
   * Checks if the neccesary atributtes to query JdownloaderClient are established
   */
  protected checkIfConnected(): void {
    if (!this.sessionToken || !this.regainToken || !this.serverEncryptionToken || !this.deviceEncryptionToken) {
      throw new Error('You are not connected. First you must call "client.connect()"');
    }
  }

  protected async postQuery<T>(url: string, data?: any): Promise<T> {
    const options: AxiosRequestConfig = {
      method: 'POST',
      /*headers: {
        'Content-Type': 'application/aesjson-jd; charset=utf-8',
      },*/
    };
    return (await axios.post(url, data, options)).data;
  }

  /**
   * Call the server with an action
   * @param {string} query The query to make to the server
   * @param {Buffer} key The singing key
   * @param {Object} params The parameters to the query
   */
  protected async callServer<T extends RidResponse>(query: string, key: Buffer, params: object): Promise<T> {
    const rid = CoreUtils.uniqueRid();

    const path = query + '?' + querystring.stringify({ ...params, rid });

    const signature = crypto
      .createHmac('sha256', key)
      .update(path)
      .digest('hex');

    try {
      const parsedBody = (await axios.post(`${ENDPOINT}${path}&signature=${signature}`)).data;
      const result = CryptoUtils.decrypt(parsedBody, key);
      const resultJson = CoreUtils.parse<T>(result);
      CoreUtils.validateRid(resultJson, rid);

      return resultJson;
    } catch (error) {
      throw CoreUtils.errorParser(error);
    }
  }

  async callAction<T>(action: string, deviceId: string, params?: any[]): Promise<DataResponse<T>> {
    this.checkIfConnected();

    const rid = CoreUtils.uniqueRid();

    const body = CryptoUtils.encrypt(JSON.stringify(this.createBody(rid, action, params)), this.deviceEncryptionToken);

    const url = `${ENDPOINT}/t_${encodeURI(this.sessionToken)}_${encodeURI(deviceId)}${action}`;

    try {
      const parsedBody = (await axios.post(url, body)).data;
      const result = CryptoUtils.decrypt(parsedBody, this.deviceEncryptionToken);
      const resultJson = CoreUtils.parse<DataResponse<T>>(result);
      CoreUtils.validateRid(resultJson, rid);
      return resultJson;
    } catch (error) {
      const err = CoreUtils.errorParser(error);
      throw new Error(CryptoUtils.decrypt(err.message, this.deviceEncryptionToken));
    }
  }

  private createBody(rid: number, query: string, params?: any[]) {
    const baseBody = {
      apiVer: API_VER,
      rid,
      url: query,
    };
    return params ? { ...baseBody, params } : baseBody;
  }

  async connect(): Promise<boolean> {
    const val = await this.callServer<ConnectResponse>('/my/connect', this.loginSecret, {
      appkey: APPKEY,
      email: this.email,
    });
    this.sessionToken = val.sessiontoken;
    this.regainToken = val.regaintoken;

    this.serverEncryptionToken = CryptoUtils.createEncryptionToken(this.loginSecret, this.sessionToken);
    this.deviceEncryptionToken = CryptoUtils.createEncryptionToken(this.deviceSecret, this.sessionToken);
    return true;
  }

  async reconnect(): Promise<boolean> {
    const val = await this.callServer<ConnectResponse>('/my/reconnect', this.serverEncryptionToken, {
      appkey: APPKEY,
      sessiontoken: this.sessionToken,
      regaintoken: this.regainToken,
    });

    this.sessionToken = val.sessiontoken;
    this.regainToken = val.regaintoken;

    this.serverEncryptionToken = CryptoUtils.createEncryptionToken(this.serverEncryptionToken, this.sessionToken);
    this.deviceEncryptionToken = CryptoUtils.createEncryptionToken(this.deviceSecret, this.sessionToken);
    return true;
  }

  async disconnect(): Promise<void> {
    await this.callServer('/my/disconnect', this.serverEncryptionToken, {
      sessiontoken: this.sessionToken,
    });

    this.sessionToken = null;
    this.regainToken = null;
    this.serverEncryptionToken = null;
    this.deviceEncryptionToken = null;
  }

  async listDevices(): Promise<Device[]> {
    this.checkIfConnected();

    const val = await this.callServer<DevicesResponse>('/my/listdevices', this.serverEncryptionToken, {
      sessiontoken: this.sessionToken,
    });

    return val.list;
  }
}
