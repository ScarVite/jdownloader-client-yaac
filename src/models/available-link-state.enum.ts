export enum AvailableLinkState {
  ONLINE = 'ONLINE',
  OFFLINE = 'OFFLINE',
  UNKNOWN = 'UNKNOWN',
  TEMP_UNKNOWN = 'TEMP_UNKNOWN',
}
