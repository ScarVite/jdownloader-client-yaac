export interface LinkVariant {
  iconKey: string;
  id: string;
  name: string;
}
