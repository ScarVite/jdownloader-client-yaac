import { AvailableLinkState } from './available-link-state.enum';
import { Priority } from './priority.enum';
import { LinkVariant } from './link-variant.interface';

export interface CrawledLink {
  availability: AvailableLinkState;
  bytesTotal: number;
  comment: string;
  downloadPassword: string;
  enabled: boolean;
  host: string;
  name: string;
  packageUUID: number;
  priority: Priority;
  url: string;
  uuid: number;
  variant: LinkVariant;
  variants: boolean;
}
